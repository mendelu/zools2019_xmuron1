//
// Created by xmuron1 on 08.04.2019.
//

#include "Ucitel.h"

Ucitel::Ucitel(std::string jmeno, std::string ustav) : Osoba(jmeno){
    m_jmenoUstavu = ustav;
}

std::string Ucitel::getUstav() {
    return m_jmenoUstavu;
}