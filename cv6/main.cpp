#include <iostream>
#include "Student.h"
#include "IS.h"
#include <vector>

int main() {
    Student* s1 = new Student("Mikulas",100);
    Student* s2 = new Student("Eva",200);
    IS* is1 = new IS("MENDELU");
    is1->pridejStudenta(s1);
    is1->pridejStudenta(s2);
    is1->vypisStudenty();
    Ucitel* u1 = new Ucitel("David","UI");
    is1->pridejUcitele(u1);
    is1->vypisUcitele();


    return 0;
}