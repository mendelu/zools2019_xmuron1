//
// Created by xmuron1 on 08.04.2019.
//

#ifndef UNTITLED_UCITEL_H
#define UNTITLED_UCITEL_H

#include "Osoba.h"

class Ucitel : public Osoba{
private:
    std::string m_jmenoUstavu;
public:
    Ucitel(std::string jmeno, std::string ustav);
    std::string getUstav();
};


#endif //UNTITLED_UCITEL_H
