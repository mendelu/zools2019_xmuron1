//
// Created by xmuron1 on 08.04.2019.
//

#include "IS.h"

IS::IS(std::string jmenoSkoly) {
    m_jmenoSkoly = jmenoSkoly;
}

void IS::pridejStudenta(Student *novyStudent) {
    m_seznamStudentu.push_back(novyStudent);
}

void IS::vypisStudenty() {
    for(Student* s : m_seznamStudentu){
        std::cout << s->getJmeno() << std::endl;
    }
}

int IS::getSumaStipendii() {
    int sum = 0;
    for(Student* s : m_seznamStudentu){
        sum = sum + s->getStipendium();
    }
    return sum;
}

void IS::pridejUcitele(Ucitel *novyUcitel) {
    m_seznamUcitelu.push_back(novyUcitel);
}

void IS::vypisUcitele(){
    for(Ucitel* s : m_seznamUcitelu){
        std::cout << s->getJmeno() << std::endl;
    }
}