#include <iostream>

class Auto{
public:
    std::string m_SPZ;
    int m_najeto;
    std::string m_pred_majitel;

    Auto(){
        m_SPZ = "xxxxx";
        m_najeto = 0;
        m_pred_majitel = "neznamy";
    }

    Auto(   std::string SPZ,
            int najeto,
            std::string majitel){

        m_SPZ = SPZ;
        m_najeto = najeto;
        m_pred_majitel = majitel;
    }

    std::string getSPZ() {
        return m_SPZ;
    }

    int getNajeto(){
        return m_najeto;
    }

    std::string getPredchoziMajitel(){
        return m_pred_majitel;
    }

    void printInfo(){
        std::cout << "Jsem auto a mam najeto "
        << m_najeto << " moje SPZ je "
        << m_SPZ << " a vlastil me "
        << m_pred_majitel << std::endl;
    }

    void setNajeto(int najeto){
        m_najeto = najeto;
    }

    void setPredchoziMajitel(std::string majitel){
        m_pred_majitel = majitel;
    }

    void setSPZ(std::string spz){
        m_SPZ = spz;
    }
};

int main() {
    Auto* a1 = new Auto("OPA1234",5000,"Mikulas");
    a1->setSPZ("ABC123");
    a1->printInfo();
}