#ifndef TEZKAUTOCNAHELMA_H
#define TEZKAUTOCNAHELMA_H
#include <iostream>
#include "Helma.h"

using namespace std;
namespace rytiri {
	class TezkaUtocnaHelma : public rytiri::Helma {


	public:
		TezkaUtocnaHelma(int velikost);

		int getBonusObrany();

		void printInfo();
	};
}
#endif // TEZKAUTOCNAHELMA_H

