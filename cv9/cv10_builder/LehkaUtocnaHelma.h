#ifndef LEHKAUTOCNAHELMA_H
#define LEHKAUTOCNAHELMA_H
#include <iostream>
#include "Helma.h"

using namespace std;
namespace rytiri {
	class LehkaUtocnaHelma : public rytiri::Helma {


	public:
		LehkaUtocnaHelma(int velikost);

		int getBonusObrany();

		void printInfo();
	};
}
#endif // LEHKAUTOCNAHELMA_H

