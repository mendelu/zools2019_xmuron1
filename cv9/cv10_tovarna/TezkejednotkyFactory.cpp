#include "TezkejednotkyFactory.h"

rytiri::Rytir* rytiri::TezkejednotkyFactory::getRytir(string jmeno, int sila) {
	return new TezkoOdenec(jmeno,sila,m_silaZbroje);
}

rytiri::TezkejednotkyFactory::TezkejednotkyFactory(int silaZbrojeTezkoodence) {
	m_silaZbroje = silaZbrojeTezkoodence;
}
