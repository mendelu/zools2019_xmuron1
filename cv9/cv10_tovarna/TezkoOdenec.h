#ifndef TezkoOdenec_H
#define TezkoOdenec_H

#include "Rytir.h"

namespace rytiri {
	class TezkoOdenec : public rytiri::Rytir {

	public:
		int m_silaZbroje;

		TezkoOdenec(string jmeno, int sila, int silaZbroje);

		int getUtok();

		int getObrana();

		void setSilaZbroje(int silaZbroje);
	};
}
#endif // TezkoOdenec_H
