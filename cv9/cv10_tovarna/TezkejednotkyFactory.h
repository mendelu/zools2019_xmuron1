#ifndef TezkejednotkyFactory_H
#define TezkejednotkyFactory_H

#include "JednotkyFactory.h"
#include "TezkoOdenec.h"

namespace rytiri {
	class TezkejednotkyFactory : public rytiri::JednotkyFactory {

	public:
		int m_silaZbroje;

		rytiri::Rytir* getRytir(string jmeno, int sila);

		TezkejednotkyFactory(int silaZbrojeTezkoodence);
	};
}
#endif // TezkejednotkyFactory_H
