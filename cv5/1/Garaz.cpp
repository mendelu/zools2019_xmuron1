//
// Created by xmuron1 on 01.04.2019.
//

#include "Garaz.h"

Garaz::Garaz(std::string adresa) {
    m_adresa = adresa;
    // pro jedno auto
    //m_automobil = nullptr;
    vyprazdniGaraz();

}

std::string Garaz::getAdresa() {
    return m_adresa;
}

void Garaz::zaparkuj(Automobil *automobil, int kam) {
    m_seznam_aut[kam] = automobil;
}

void Garaz::vyprazdniGaraz() {
    for(Automobil* &automobil: m_seznam_aut){
        automobil = nullptr;
    }
}

void Garaz::printInfo() {
    std::cout << "Jsem garaz na adrese " << getAdresa() << std::endl;
    for(Automobil* automobil : m_seznam_aut){
        if(automobil == nullptr){
            std::cout << "Na pozici neni zadne auto" << std::endl;
        }
        else{
            std::cout << "Je tam automobil modelu ";
            std::cout << automobil->getModel() << std::endl;
        }
    }
    /*
    if(m_automobil == nullptr){
        std::cout << "V garazi neni zadne auto." << std::endl;
    }
    else{
        std::cout << "V garazi je auto modelu ";
        std::cout << m_automobil->getModel() << std::endl;
    }
     */
}