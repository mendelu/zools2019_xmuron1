#include <iostream>
#include "Garaz.h"

int main() {
    Automobil* a1 = new Automobil("Skoda");
    Garaz* g = new Garaz("Petra Bezruce");
    g->zaparkuj(a1,1);
    g->printInfo();

    // Pro jedno
    //g->zaparkuj(a1);
    return 0;
}