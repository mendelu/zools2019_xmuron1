#ifndef UNTITLED_AUTOMOBIL_H
#define UNTITLED_AUTOMOBIL_H

#include <iostream>

class Automobil {
private:
    std::string m_model;
public:
    Automobil(std::string model);
    std::string getModel();
};


#endif //UNTITLED_AUTOMOBIL_H
