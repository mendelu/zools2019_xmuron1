//
// Created by xmuron1 on 01.04.2019.
//

#ifndef UNTITLED_GARAZ_H
#define UNTITLED_GARAZ_H

#include <iostream>
#include <array>
#include "Automobil.h"

class Garaz {
private:
    std::string m_adresa;
    // Pro jedno auto
    // Automobil* m_automobil;

    // Pro vice aut
    std::array<Automobil*,5> m_seznam_aut;

public:
    Garaz(std::string adresa);
    std::string getAdresa();
    void zaparkuj(Automobil* automobil, int kam);
    void vyprazdniGaraz();
    void printInfo();
};


#endif //UNTITLED_GARAZ_H
