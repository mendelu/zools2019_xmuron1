//
// Created by David Prochazka on 15/11/2018.
//

#include "Zamestnanec.h"

Zamestnanec::Zamestnanec(std::string jmeno, int letVeSpolecnosti, int dosazeneVzdelani, PoziceZamestnance pozice){
    m_jmeno = jmeno;
    m_dosazeneVzdelani = dosazeneVzdelani;
    m_letVeSpolecnosti = letVeSpolecnosti;
    m_cerpanoDniDovolene = 0;

    if (pozice == PoziceZamestnance::Akademik){
        m_pozice = new Akademik();
    } else {
        m_pozice = new Technik();
    }
}

void Zamestnanec::zmenPoziciZamestnance(PoziceZamestnance novaPozice){
    delete m_pozice;
    // shodou okolnosti je to podobny kod jako v konstruktoru, muzeme refactorovat do pomocne metody
    // ale az sekundarne
    if (novaPozice == PoziceZamestnance::Akademik){
        m_pozice = new Akademik();
    } else {
        m_pozice = new Technik();
    }
}

int Zamestnanec::getPlat(){
    return m_pozice->getPlat(m_dosazeneVzdelani, m_letVeSpolecnosti);
}

int Zamestnanec::getPocetDniDovolene(){
    return m_pozice->getPoceDniDovoleni(m_cerpanoDniDovolene);
}

int Zamestnanec::evidujDovolenou(int dniDovolene){
    m_cerpanoDniDovolene += dniDovolene;
}
