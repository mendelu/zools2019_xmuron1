//
// Created by David Prochazka on 15/11/2018.
//

#include "Technik.h"

int Technik::getPlat(int stupenVzdelani, int letVeSpolecnosti){
    return s_platovyZaklad + s_rocniBous * letVeSpolecnosti;
}

int Technik::getPoceDniDovoleni(int cerpanoDni){
    return s_maxDniDovolene - cerpanoDni;
}
