#ifndef PRIKAZ_H
#define PRIKAZ_H
#include <iostream>
#include "Lektvar.h"
#include "Hrdina.h"

using namespace std;
class Prikaz {

public:
	virtual void PouzijLektvar(Lektvar* lektvar, Hrdina* hrdina) = 0;

	virtual string getPopis() = 0;
};
#endif // PRIKAZ_H

