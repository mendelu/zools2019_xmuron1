#include "Hrdina.h"
#include "Prikaz.h"

Hrdina::Hrdina(int zivot) {
    m_zivot = zivot;
}

void Hrdina::vypisLektvary() {
    if(m_lektvary.size() > 0){
        for(int i = 0; i < m_lektvary.size(); i++){
            m_lektvary.at(i)->printInfo();
        }
    }else{
        cout<<"nejsou zadne lektvary"<<endl;
    }
}

void Hrdina::vypisPrikazy() {
    if(m_prikazy.size() > 0){
        for(int i = 0; i < m_prikazy.size(); i++){
            cout << i << ". "<< m_prikazy.at(i)->getPopis() << endl;
        }
    }else{
        cout<<"nenacuil ses zatim zadne prikazy"<<endl;
    }
}

void Hrdina::seberLektvar(Lektvar* jaky) {
    m_lektvary.push_back(jaky);
}

void Hrdina::pouzijLektvar() {
    int indexLektvar = 0;
    int indexPrikaz = 0;

    cout<<"mas nasledujici lektvary"<<endl;
    vypisLektvary();

    cout<<"mas nasledujici prikazy"<<endl;
    vypisPrikazy();

    cout<<"jaky chces pouzit lektvar?"<<endl;
    cin>>indexLektvar;

    cout<<"jaky chces pouzit prikaz?"<<endl;
    cin>>indexPrikaz;

    Lektvar* pomLektvar = m_lektvary.at(indexLektvar);
    m_prikazy.at(indexPrikaz)->PouzijLektvar(pomLektvar, this);

    if(pomLektvar->getBonusZivot() <= 0){
        cout<<"vyrazuji prazdny lektvar"<<endl;
        m_lektvary.erase(m_lektvary.begin() + indexLektvar);
    }
}

void Hrdina::naucSePrikaz(Prikaz* jaky) {
    m_prikazy.push_back(jaky);
}

int Hrdina::getZivot() {
    return m_zivot;
}

void Hrdina::setZivot(int kolik) {
    m_zivot += kolik;
}
