#ifndef PRIKAZPROHLEDNI_H
#define PRIKAZPROHLEDNI_H
#include <iostream>
#include "Prikaz.h"

using namespace std;
class PrikazProhledni : public Prikaz {

public:
	void PouzijLektvar(Lektvar* lektvar, Hrdina* hrdina);

	string getPopis();
};
#endif // PRIKAZPROHLEDNI_H

