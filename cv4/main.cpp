#include <iostream>

enum class SilaHrdiny{
    slaby, silny
};

class Hrdina{
private:
    int m_sila;
    int m_zivot;
    std::string m_jmeno;
    static int s_pocet;
public:
    Hrdina(int sila, int zivot, std::string jmeno){
        m_jmeno = jmeno;
        m_sila = sila;
        m_zivot = zivot;
        s_pocet = s_pocet+1;
    }

    ~Hrdina(){
        s_pocet = s_pocet - 1;
    }

    static int getPocetHrdinu(){
        return s_pocet;
    }

    int getSila(){
        return m_sila;
    }

    void snizZivoty(int kolik){
        m_zivot = m_zivot - kolik;
        if(m_zivot < 0){
            std::cout << getJmeno() << " je mrtvy" << std::endl;
            m_zivot = 0;
        }
    }

    void zautoc(Hrdina* nepritel){
        nepritel->snizZivoty(getSila());
        std::cout << getJmeno() << " utoci na " << nepritel->getJmeno() << " a sebral " << getSila() << "zivotu" << std::endl;

    }

    void printInfo(){
        std::cout << "Jmeno: " << getJmeno() << std::endl;
        std::cout << "Zivoty: " << m_zivot << std::endl;
        std::cout << "Sila: " << getSila() << std::endl;
    }

    std::string getJmeno(){
        return m_jmeno;
    }

    static Hrdina* vytvorHrdinu(SilaHrdiny sila){
        if(sila == SilaHrdiny::silny) {
            return new Hrdina(100, 1000, "Silny");
        }
        else{
            return new Hrdina(1,10,"Slabeho");
        }
    }

};

int Hrdina::s_pocet = 0;

int main() {
    Hrdina* h1 = Hrdina::vytvorHrdinu(SilaHrdiny::silny);
    Hrdina* h2 = new Hrdina(20,200,"Eva");
    h1->zautoc(h2);
    std::cout << Hrdina::getPocetHrdinu();
    return 0;
}