//
// Created by Mikulas Muron on 26/11/2018.
//

#ifndef CV10_STUDENT_H
#define CV10_STUDENT_H

#include <iostream>

using std::string;

enum class OborStudia{
    ARI, EI, HPS, EM
};

class Student {
protected:
    string m_jmeno;
    OborStudia m_obor;
    int m_studujeLet;
    float m_studijniPrumer;

public:
    Student(string jmeno, OborStudia obor, int studujeLet, int prumer);
    string getJmeno();
    OborStudia getObor();
    int getPocetLetStudia();
    float getPrumer();
    void pridejKOdstudovanymLetum(int kolikLet);
    void setStudijniPrumer(float novyPrumer);

    // implementuji potomci
    virtual int getStipendium() = 0;
    virtual bool prodluzujeStudium() = 0;
    virtual void printInfo() = 0;

    // tovarni metoda
    static Student* getStudent(string jmeno, int dobaStudia, OborStudia obor, int dojezdovaVzdalenost);

};


#endif //CV10_STUDENT_H
